﻿using MySql.Data.MySqlClient;
using System;
using System.Windows;
using System.Windows.Controls;

namespace WPFAppWithDatabaseTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string name = "";
        private int age = 0;
        private string gender = "";
        private string mysqlconnectionstring = null;
        private MySqlConnection conn;
        private string query = null;

        public MainWindow()
        {
            //Initialization during software startup

            InitializeComponent();
            buttonUpdate.IsEnabled = false;
            buttonDelete.IsEnabled = false;
            if (checkDBConn())
            {
                dropdownID();
            }
        }

        private bool checkDBConn()
        {
            //Check DB Connection during software run

            mysqlconnectionstring = "server=localhost;database=sample_database;uid=root;pwd=;";
            using (conn = new MySqlConnection(mysqlconnectionstring))
            {
                try
                {
                    conn.Open();
                    //MessageBox.Show("Connection to database is OK!");
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Unable to connect to the database");
                    return false;
                    throw;
                }
            }
            return true;
        }

        private void buttonCreateUser_Click(object sender, RoutedEventArgs e)
        {
            //Action on clicking Create User button
            //Check textbox and catch error
            //If passed, do query

            if (!string.IsNullOrEmpty(formName.Text))
                name = formName.Text;
            else
                name = "No Name";

            if (!string.IsNullOrEmpty(formGender.Text))
                gender = formGender.Text;
            else
                gender = "Genderless";

            age = parseThisToInt(formAge.Text);

            if (name == "No Name" || gender == "Genderless" || age == 0)
                MessageBox.Show("Please enter your name, age and gender first before submitting");
            else
            {
                query = "INSERT INTO list_people(NAME,AGE,GENDER) " +
                "VALUES ('" + name + "','" + age + "','" + gender + "');";
                doQueryToDB(query);
                MessageBox.Show("Successfully created user in database");
            }

            dropdownID();
            formName.Clear();
            formAge.Clear();
            formGender.Clear();
        }

        private int parseThisToInt(String textBoxString)
        {
            //From textbox, try converting string to int type

            int parseResult;
            bool isParsable = Int32.TryParse(textBoxString, out parseResult);
            if (isParsable)
                return parseResult;
            else
                return 0;
        }

        private void doQueryToDB(string query)
        {
            ///Responsible in sending sql query to database (CRUD)

            mysqlconnectionstring = "server=localhost;database=sample_database;uid=root;pwd=;";
            using (conn = new MySqlConnection(mysqlconnectionstring))
            {
                using (MySqlCommand executeQuery = new MySqlCommand(query, conn))
                {
                    MySqlDataReader Reader;
                    try
                    {
                        conn.Open();
                        Reader = executeQuery.ExecuteReader();
                        conn.Close();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Unable to perform the specified action");
                        throw;
                    }
                }
            }
        }

        private void fetchNamesButton_Click(object sender, RoutedEventArgs e)
        {
            //Actions on clicking Fetch User List button
            //Sort list by ID
            //Read data from DB and put it into a string with proper formating for readability
            //The string will then be added into the box
            //Using Select query first, then use DataReader
            //Error catching in case DB Connection lost on button click

            sortID();
            string listHolder = "";
            mysqlconnectionstring = "server=localhost;database=sample_database;uid=root;pwd=;";
            query = "SELECT * FROM list_people";
            nameListBox.Items.Clear();
            using (conn = new MySqlConnection(mysqlconnectionstring))
            {
                using (MySqlCommand executeQuery = new MySqlCommand(query, conn))
                {
                    try
                    {
                        conn.Open();
                        MySqlDataReader Reader = executeQuery.ExecuteReader();
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                listHolder = Reader.GetString(0) + "--"
                                    + Reader.GetString(1) + " || "
                                    + Reader.GetString(2) + " || "
                                    + Reader.GetString(3);
                                nameListBox.Items.Add(listHolder);
                            }
                            Reader.Close();
                        }
                        MessageBox.Show("Successfully fetched the latest list of names");
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Error in fetching names. Check database connection");
                        throw;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        private void sortID()
        {
            //Relational DB cannot do autosorting.
            //Use Alter Table query

            query = "ALTER TABLE list_people ORDER BY id;";
            doQueryToDB(query);
        }

        private void checkDBButton_Click(object sender, RoutedEventArgs e)
        {
            //Check DB connection button
            if (checkDBConn())
            {
                MessageBox.Show("Connection to database is OK!");
                dropdownID();
            }
        }

        private void dropdownID()
        {
            //Fetch ID from DB, to be used for updating user details
            //Uses Select query
            //If there is already an item in the ID box, clear it
            //Using DataReader to read the data and write them into ID box
            //Catch error

            mysqlconnectionstring = "server=localhost;database=sample_database;uid=root;pwd=;";
            query = "SELECT * FROM list_people";
            using (conn = new MySqlConnection(mysqlconnectionstring))
            {
                using (MySqlCommand executeQuery = new MySqlCommand(query, conn))
                {
                    try
                    {
                        conn.Open();
                        if (listIDEdit.Items.Count >= 1)
                        {
                            listIDEdit.Items.Clear();
                        }
                        MySqlDataReader Reader = executeQuery.ExecuteReader();
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                //MessageBox.Show(Reader.GetString(0));
                                listIDEdit.Items.Add(Reader.GetString(0));
                            }
                            Reader.Close();
                        }
                        //MessageBox.Show("Successfully fetched the latest list of names");
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Error in fetching ID. Check database connection");
                        throw;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        private void listIDEdit_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Disable/Enable Update User and Delete User button. Enable -> Only after ID is selected.
            //Preventing user from updating or deleting without selecting ID

            if (listIDEdit.SelectedItem != null && !string.IsNullOrEmpty(listIDEdit.SelectedItem.ToString()))
            {
                buttonUpdate.IsEnabled = true;
                buttonDelete.IsEnabled = true;
            }
            else
            {
                buttonUpdate.IsEnabled = false;
                buttonDelete.IsEnabled = false;
            }
        }

        private void formName_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void formAge_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void formGender_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            //Action on clicking Update User button
            //Checks textbox for valid data
            //Prepares sql query before sending to doQueryToDB()
            //Prepares sql query only for box with valid user input
            //Clears box

            string appendNameToQuery, appendAgeToQuery, appendGenderToQuery;
            appendNameToQuery = appendAgeToQuery = appendGenderToQuery = " ";

            bool readyUpdateName, readyUpdateGender, readyUpdateAge;
            readyUpdateName = readyUpdateGender = readyUpdateAge = false;

            string updateMessage = "";

            string currentIDSelection = listIDEdit.Text;

            if (!string.IsNullOrEmpty(formNameEdit.Text))
            {
                name = formNameEdit.Text;
                appendNameToQuery = $"set NAME='{name}'";
                readyUpdateName = true;
            }

            if (!string.IsNullOrEmpty(formGenderEdit.Text))
            {
                gender = formGenderEdit.Text;
                appendGenderToQuery = $"set GENDER = '{gender}'";
                readyUpdateGender = true;
            }

            while (!string.IsNullOrEmpty(formAgeEdit.Text))
            {
                age = parseThisToInt(formAgeEdit.Text);
                if (age == 0)
                    break;
                appendAgeToQuery = $"set AGE='{age}'";
                readyUpdateAge = true;
                break;
            }

            if (readyUpdateName)
            {
                query = $"update list_people {appendNameToQuery} where ID='{currentIDSelection}';";
                doQueryToDB(query);
                updateMessage += "\nName ";
            }

            if (readyUpdateGender)
            {
                query = $"update list_people {appendGenderToQuery} where ID='{currentIDSelection}';";
                doQueryToDB(query);
                updateMessage += "\nGender ";
            }

            if (readyUpdateAge)
            {
                query = $"update list_people {appendAgeToQuery} where ID='{currentIDSelection}';";
                doQueryToDB(query);
                updateMessage += "\nAge ";
            }

            if (!String.IsNullOrWhiteSpace(updateMessage))
                MessageBox.Show($"Successfully update: {updateMessage}");

            formNameEdit.Clear();
            formAgeEdit.Clear();
            formGender.Clear();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            //Action on clicking Delete User

            string currentIDSelection = listIDEdit.Text;
            query = $"DELETE FROM list_people WHERE ID='{currentIDSelection}';";
            doQueryToDB(query);
            MessageBox.Show($"User with ID number {currentIDSelection} has been successfully deleted");
        }
    }
}